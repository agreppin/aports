# Contributor: August Klein <amatcoder@gmail.com>
# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: August Klein <amatcoder@gmail.com>
pkgname=meld
pkgver=3.20.4
pkgrel=0
pkgdesc="A visual diff and merge tool"
url="https://meldmerge.org"
arch="noarch !s390x !mips !mips64" # missing gtksourceview
license="GPL-2.0-or-later"
depends="dconf gtksourceview gsettings-desktop-schemas py3-gobject3
	py3-cairo"
makedepends="intltool itstool libxml2-utils"
checkdepends="py3-pytest xvfb-run gtk-update-icon-cache"
subpackages="$pkgname-lang $pkgname-doc"
source="https://download.gnome.org/sources/meld/${pkgver%.*}/meld-$pkgver.tar.xz"

prepare() {
	default_prepare

	# Needs meld icons, so we'd need to mess around with gtk discovering the icons
	# in place and generating an icon cache etc. so let's disable it for now.
	rm test/test_gutterrendererchunk.py
}

build() {
	python3 setup.py build
}

check() {
	PYTHONPATH="$PWD/build/lib" xvfb-run pytest-3
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
e7b5c07e68972e7e8e4d396140fa2ee627609728533269f1bc52e9339e89a58f0949a1e8ec7b7847eec3eff5a67ea0bd5e07ad8002d2c640d173f9b0c4aa724e  meld-3.20.4.tar.xz
"
